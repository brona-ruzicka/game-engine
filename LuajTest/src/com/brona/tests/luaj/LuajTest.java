package com.brona.tests.luaj;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;


public class LuajTest {
	
	public static void main(String[] args) throws Exception {
		String scriptFile = "test.lua";
		
		// create an environment to run in
		Globals globals = JsePlatform.standardGlobals();
		
		// Use the convenience function on Globals to load a chunk.
		LuaValue chunk = globals.loadfile(scriptFile);
		
		// Use any of the "call()" or "invoke()" functions directly on the chunk.
		chunk.call();
	}


}