package com.brona.flum.tokenizer;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer {

    private ArrayList<TokenData> tokenData;

    private String code;

    private Token lastToken;
    private boolean pushBack;

    public Tokenizer(String code) {
        this.code = code;

        this.tokenData = new ArrayList<>();

        tokenData.add(new TokenData(Pattern.compile("^(false|true)"),                                       TokenType.BOOL_LITERAL      ));
        tokenData.add(new TokenData(Pattern.compile("^((-?[0-9]+f)|(-?[0-9]+\\.[0-9]+f?)|(\\.[0-9]+f?))"),  TokenType.DEC_LITERAL       ));
        tokenData.add(new TokenData(Pattern.compile("^-?[0-9]+"),                                           TokenType.INT_LITERAL       ));
        tokenData.add(new TokenData(Pattern.compile("^\".*\""),                                             TokenType.STRING_LITERAL    ));

        tokenData.add(new TokenData(Pattern.compile("^[A-Za-z][\\w]*"), TokenType.IDENTIFIER));

        for(String token : new String[] { "^,", "^\\.", "^->", "^-" , "^\\+", "^\\*", "^/", "^%", "^!=", "^!", "^>=", "^>", "^<=", "^<", "^==", "^=", "^\\|\\|", "^\\|", "^&&", "^&", "^<<", "^>>", "^\\(", "^\\)", "^\\[", "^\\]", "^\\{", "^\\}"}) {
            tokenData.add(new TokenData(Pattern.compile(token), TokenType.TOKEN));
        }
    }

    public Token nextToken() {
        code = code.trim();

        if(pushBack) {
            pushBack = false;
            return lastToken;
        }

        if(code.isEmpty()) {
            return (lastToken = new Token("", TokenType.EMPTY));
        }

        for(TokenData data : tokenData) {
            Matcher matcher = data.getPattern().matcher(code);
            if(matcher.find()) {
                String token = matcher.group().trim();
                code = matcher.replaceFirst("");

                if(data.getType() == TokenType.STRING_LITERAL) {
                    return(lastToken = new Token(token.substring(1, token.length() - 1), TokenType.STRING_LITERAL));
                } else if(data.getType() == TokenType.DEC_LITERAL) {
                    return(lastToken = new Token((token.charAt(token.length() - 1) == 'f') ? token.substring(0, token.length() - 1) : token, TokenType.DEC_LITERAL));
                } else {
                    return(lastToken = new Token(token, data.getType()));
                }

            }
        }

        throw new IllegalStateException("Unresolvable token: " + code);
    }

    public boolean hasNextToken() {
        return !code.isEmpty();
    }

    public void pushBack() {
        if(lastToken != null)
            pushBack = true;
    }

}
