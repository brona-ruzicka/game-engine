package com.brona.flum.tokenizer;

public class Test {

    public static void main(String[] args) {

        String code = "class HelloWorld { func ahoj = (int low, func mapping) -> void { return object.callableFunction2() } }";

        Tokenizer tokenizer = new Tokenizer(code);

        while(tokenizer.hasNextToken()) {

            Token token =  tokenizer.nextToken();

            System.out.println("Found new token \"" + token.getToken() + "\" of type " + token.getType());

        }

    }
}
