package com.brona.flum;

import java.util.ArrayList;

public abstract class Block extends Command {

    protected ArrayList<Command> subCommands;

    public Block(Block superBlock) {
        super(superBlock);

        this.subCommands = new ArrayList<>();
    }

    abstract void run();
}
