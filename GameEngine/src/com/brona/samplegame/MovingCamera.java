package com.brona.samplegame;

import java.awt.event.KeyEvent;

import com.brona.gameengine.Engine;
import com.brona.gameengine.GameInterface;
import com.brona.gameengine.Renderer.Camera;

public class MovingCamera extends Camera{

	public static final int STEP = 100; 
	
	public MovingCamera(int offsetX, int offsetY) {
		super.offsetX = offsetX;
		super.offsetY = offsetY;		
	}
	
	@Override
	public void init(Engine engine, GameInterface game) {
		engine.getRenderer().setCamera(this);
	}
	
	@Override
	public void update(Engine engine, GameInterface game, float deltaTime) {
		if(engine.getInput().isKey(KeyEvent.VK_UP)) super.offsetY = super.offsetY - Math.round(STEP  * deltaTime);
		if(engine.getInput().isKey(KeyEvent.VK_DOWN)) super.offsetY = super.offsetY + Math.round(STEP  * deltaTime);
		if(engine.getInput().isKey(KeyEvent.VK_LEFT)) super.offsetX = super.offsetX - Math.round(STEP  * deltaTime);
		if(engine.getInput().isKey(KeyEvent.VK_RIGHT)) super.offsetX = super.offsetX + Math.round(STEP  * deltaTime);
	}
	
}
