package com.brona.gameengine.renderables;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.brona.gameengine.Renderer.Renderable;

public class Image extends Renderable {

	public Image() {
		super();
	}

	public Image(String path) {
		this(path, true);
	}

	public Image(String path, boolean isTransparent) {

		BufferedImage image = null;
		try {
			image = ImageIO.read(Image.class.getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}

		super.width = image.getWidth();
		super.height = image.getHeight();
		super.pixels = image.getRGB(0, 0, super.width, super.height, null, 0, super.width);

		image.flush();

		super.isTransparent = isTransparent;
	}

	public Image setWidth(int width) {
		super.width = width;
		return this;
	}

	public Image setHeight(int height) {
		super.height = height;
		return this;
	}

	public Image setPixels(int[] pixels) {
		super.pixels = pixels;
		return this;
	}

	public Image setIsTransparent(boolean isTransparent) {
		super.isTransparent = isTransparent;
		return this;
	}

}
