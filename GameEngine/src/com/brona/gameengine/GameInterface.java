package com.brona.gameengine;

public interface GameInterface {

	public void init(Engine engine);
	
	public void update(Engine engine, float deltaTime, int fps);

	public void render(Engine engine, Renderer renderer);

}
