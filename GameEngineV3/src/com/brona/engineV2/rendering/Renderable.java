package com.brona.engineV2.rendering;

public class Renderable {

	private int width, height;
	private byte[] pixels;
	
	public Renderable() {
		width = 0;
		height = 0;
		pixels = new byte[0];
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public byte[] getPixels() {
		return pixels;
	}
	
}
