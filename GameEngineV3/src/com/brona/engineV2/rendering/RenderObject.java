package com.brona.engineV2.rendering;

class RenderObject {

	private int offsetX, offsetY;
	private int width, height;
	private boolean statical;
	private int layer;
	private byte[] pixels;
	
    RenderObject(Renderable renderable, int offsetX, int offsetY, int layer) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		
		this.width = renderable.getWidth();
		this.height = renderable.getWidth();
		
		this.layer = layer;
		
		this.pixels = renderable.getPixels().clone();
	}
    
    void copy(RenderObject object) {
    	this.offsetX = object.offsetX;
    	this.offsetY = object.offsetY;
    	
    	this.width = object.width;
    	this.height = object.height;
    	
    	this.layer = object.layer;
    	
    	this.pixels = object.pixels.clone();
    }

	int getOffsetX() {
		return offsetX;
	}

	int getOffsetY() {
		return offsetY;
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	int getLayer() {
		return layer;
	}
	
	byte[] getPixels() {
		return pixels;
	}

	void setOffsetX(int offsetX) {
		this.offsetX = offsetX;
	}

	void setOffsetY(int offsetY) {
		this.offsetY = offsetY;
	}

	void setWidth(int width) {
		this.width = width;
	}

	void setHeight(int height) {
		this.height = height;
	}
	
	void setLayer(int layer) {
		this.layer = layer;
	}

	void setPixels(byte[] pixels) {
		this.pixels = pixels.clone();
	}
	
	void setRenderable(Renderable renderable) {
		this.width = renderable.getWidth();
		this.height = renderable.getWidth();
		
		this.pixels = renderable.getPixels().clone();
	}
}
