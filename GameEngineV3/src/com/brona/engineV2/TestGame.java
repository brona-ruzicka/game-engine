package com.brona.engineV2;

import com.brona.engineV2.rendering.Renderer;

public class TestGame implements GameInterface {

	@Override
	public void init(EngineContainer ec) {
		System.out.println(Thread.currentThread().getName() + ": GAME-INIT");

	}

	@Override
	public void update(Input input, double deltaTime) {
		System.out.println(Thread.currentThread().getName() + ": GAME-UPDATE: " + (int)Math.round(1 / deltaTime) + ", " + deltaTime);		
	}

	@Override
	public void render(Renderer renderer) {
		System.out.println(Thread.currentThread().getName() + ": GAME-RENDER");
	}

	public static void main(String[] args) {
		EngineContainer ec = new EngineContainer(new TestGame());
		ec.setWidth(16);
		ec.setHeight(9);
		ec.setScale(20f);
		ec.setFps(60);
		ec.start();

	}

}
