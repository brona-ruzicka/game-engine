package com.brona.engineV2;

import java.awt.BorderLayout;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class Window {

	private JFrame frame;
	private BufferedImage image;
	private Canvas canvas;
	private BufferStrategy bufferStrategy;
	private Graphics graphics;

	public Window(EngineContainer ec) {
		image = new BufferedImage(ec.getWidth(), ec.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		canvas = new Canvas();
		Dimension s = new Dimension((int) (ec.getWidth() * ec.getScale()),
				(int) (ec.getHeight() * ec.getScale()));
		canvas.setPreferredSize(s);
		canvas.setMinimumSize(s);
		canvas.setMaximumSize(s);

		frame = new JFrame(ec.getTitle());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(this.canvas, BorderLayout.CENTER);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);

		//this.frame.getContentPane().setCursor(java.awt.Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new java.awt.Point(0, 0), "blank cursor"));
		
		canvas.createBufferStrategy(2);
		canvas.requestFocus();
		bufferStrategy = this.canvas.getBufferStrategy();
		graphics = this.bufferStrategy.getDrawGraphics();

	}

	public void update() {
		graphics.drawImage(this.image, 0, 0, this.canvas.getWidth(), this.canvas.getHeight(), null);
		bufferStrategy.show();
	}

	public BufferedImage getImage() {
		return image;
	}

	public Canvas getCanvas() {
		return canvas;
	}
}
