package com.brona.engineV2;

import com.brona.engineV2.EngineContainer;
import com.brona.engineV2.rendering.Renderer;

public interface GameInterface {

    public void init(EngineContainer ec);
	
	public void update(Input input, double deltaTime);

	public void render(Renderer renderer);
	
}
