package com.brona.engineV2;

import java.util.concurrent.Semaphore;

import com.brona.engineV2.Window;
import com.brona.engineV2.Input;
import com.brona.engineV2.rendering.Renderer;

public class EngineContainer {
	
	private Semaphore semaphore;
	private long updateCap = Math.round((1.0 / 60.0) * 1e9); //In nanoseconds 60fps
	private UpdateThread updateThread;
	private RenderThread renderThread;
	private boolean running = false;
	
	private GameInterface game;
	private Window window;
	private Renderer renderer;
	private Input input;
	
	private int width = 1, height = 1;
	private float scale = 1f;
	private String title = "Brona's OpenGEV2 Patch-2";
	
	public EngineContainer(GameInterface game) {
		this.game = game;	
	}

	public void start() {
		running = true;
		
		window = new Window(this);
		renderer = new Renderer(this);
		input = new Input(this);
		
		semaphore = new Semaphore(1);
		updateThread = new UpdateThread(this);
		renderThread = new RenderThread(this);
		
		game.init(this);
				
		updateThread.start();
		renderThread.start();
	}
	
	public void stop() {
		running = false;
	}
	
	
	public Semaphore getSemaphore() {
		return semaphore;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public GameInterface getGame() {
		return game;
	}
	
	public Window getWindow() {
		return window;
	}

	public Input getInput() {
		return input;
	}

	public Renderer getRenderer() {
		return renderer;
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	long getUpdateCap() {
		return updateCap;
	}

	void setUpdateCap(long updateCap) {
		this.updateCap = updateCap;
	}
	
	void setFps(int fps) {
		this.updateCap = Math.round((1.0 / fps) * 1e9);
	}
}
