package com.brona.gameengine;

public class Timer {

	private long startTime;
	private long stopTime;
	private Runnable callback;
	
	public Timer(long duration, Runnable callback) {
		startTime = System.nanoTime();
		stopTime = duration + startTime;	
	    this.callback = callback;
	}

	public boolean update() {
		if(stopTime >= System.nanoTime()) callback.run();
		return hasRun();
	}
	
	public long getRemaining() {
		return ((stopTime - System.nanoTime()) > 0) ? (stopTime - System.nanoTime()) : 0;
	}
	
	public long getRuntime() {
		return ((System.nanoTime() - startTime) > 0) ? (System.nanoTime() - startTime) : 0;
	}
	
	public boolean hasRun() {
		return stopTime <= System.nanoTime();
	}
	
}