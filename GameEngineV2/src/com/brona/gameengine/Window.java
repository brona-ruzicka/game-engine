package com.brona.gameengine;

import java.awt.BorderLayout;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class Window {

	private JFrame frame;
	private BufferedImage image;
	private Canvas canvas;
	private BufferStrategy bufferStrategy;
	private Graphics graphics;

	public Window(Engine engine) {
		this.image = new BufferedImage(engine.getWidth(), engine.getHeight(), BufferedImage.TYPE_INT_RGB);
		this.canvas = new Canvas();
		Dimension s = new Dimension((int) (engine.getWidth() * engine.getScale()),
				(int) (engine.getHeight() * engine.getScale()));
		this.canvas.setPreferredSize(s);
		this.canvas.setMinimumSize(s);
		this.canvas.setMaximumSize(s);

		this.frame = new JFrame(engine.getTitle());
		this.frame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				engine.stop();
			}
			
			
		});
		this.frame.setLayout(new BorderLayout());
		this.frame.add(this.canvas, BorderLayout.CENTER);
		this.frame.pack();
		this.frame.setLocationRelativeTo(null);
		this.frame.setResizable(false);
		this.frame.setVisible(true);

		this.canvas.createBufferStrategy(3);
		this.canvas.requestFocus();
		this.bufferStrategy = this.canvas.getBufferStrategy();
		this.graphics = this.bufferStrategy.getDrawGraphics();

	}

	public void update() {
		this.graphics.drawImage(this.image, 0, 0, this.canvas.getWidth(), this.canvas.getHeight(), null);
		this.bufferStrategy.show();
	}

	public BufferedImage getImage() {
		return this.image;
	}

	public Canvas getCanvas() {
		return this.canvas;
	}
	
	public void setIcon(String path) {
		this.frame.setIconImage(Toolkit.getDefaultToolkit().getImage(Window.class.getResource(path)));
	}
}
