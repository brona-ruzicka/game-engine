package com.brona.gameengine.renderables;

public class Animation extends Image {

	private int tiles;
	private int tile = 0;
	private float spt = 1; //Second per tile
	
	public Animation() {
		super();
		this.tiles = 0;
	}
	
	public Animation(String path) {
		super(path);
		
		this.tiles = super.height / super.width;
		super.height = super.width;
	}
	
	public Animation(String path, boolean isTransparent) {
		super(path, isTransparent);
		
		this.tiles = super.height / super.width;
		super.height = super.width;
	}
	
	public Animation(String path, boolean isTransparent, float spt) {
		super(path, isTransparent);
		
		this.tiles = super.height / super.width;
		super.height = super.width;
		this.spt = spt;
	}
	
	@Override
	public int[] getPixels() {
		int[] returnPixels =  new int[super.width * super.width];
		for(int i = 0; i < returnPixels.length; ++i) {
			returnPixels[i] = super.pixels[i + this.tile * (super.width * super.width)];
		}
		return returnPixels;
	}
	
	public int getCurrentTile() {
		return this.tile;
	}

	public void setCurrentTile(int tile) {
		if((tile < this.tiles) && (tile >= 0)) this.tile = tile;
	}
	
    private float tileTime = 0;
	public void update(float deltaTime) {
		this.tileTime += deltaTime;
		while(this.tileTime > this.spt) {
			this.tileTime -= this.spt;
			this.next();
		}
	}
	
	public void next() {
		this.tile++;
		this.tile %= this.tiles;
	}
	
	public void prevoius() {
		this.tile--;
		if(this.tile < 0) this.tile = this.tiles + this.tile;
	}

	public int getTiles() {
		return this.tiles;
	}

	public void setTiles(int tiles) {
		this.tiles = tiles;
	}

	public float getSpt() {
		return this.spt;
	}

	public void setSpt(float spt) {
		this.spt = spt;
	}
	
}
