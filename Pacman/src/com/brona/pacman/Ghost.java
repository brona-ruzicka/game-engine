package com.brona.pacman;

import java.util.Random;

import com.brona.gameengine.Input;
import com.brona.gameengine.Renderer;
import com.brona.gameengine.renderables.Animation;
import com.brona.gameengine.renderables.Rectangle;

public class Ghost extends GameObject {
	
	public static final float STEP = 1f;
	public static final int GHOST_LAYER = 4; 
	
	private Animation anim, normal, eatable, dead;
	private int rotation = 0;
	private int wantedRotation;
	
	Random random;
	private Rectangle rect, rNormal, rEatable, rDead;
	
	public Ghost(float positionX, float positionY) {
		super(positionX, positionY);

		this.normal = new Animation("/pacman/images/ghost_normal.png", true);
		this.eatable = new Animation("/pacman/images/ghost_eatable.png", true);
		this.dead = new Animation("/pacman/images/ghost_dead.png", true);
		
		this.anim = this.normal;
		
		this.rNormal = new Rectangle(PacmanGameManager.TILE_SIZE, PacmanGameManager.TILE_SIZE, (java.awt.Color.RED.getRGB() & 0x00ffffff) | 0x77000000, true);
		this.rEatable = new Rectangle(PacmanGameManager.TILE_SIZE, PacmanGameManager.TILE_SIZE, (java.awt.Color.BLUE.getRGB() & 0x00ffffff) | 0x77000000, true);
		this.rDead = new Rectangle(PacmanGameManager.TILE_SIZE, PacmanGameManager.TILE_SIZE, (java.awt.Color.WHITE.getRGB() & 0x00ffffff) | 0x77000000, true);
		
		this.rect = this.rNormal;
		
		this.random = new Random();
		this.wantedRotation = this.random.nextInt(4);
	}

	private int updateCounter = 0;
	
	@Override
	public void update(Input input, float deltaTime) {
		
		if((game.getPacman().getTileX() == super.tileX) && (game.getPacman().getTileY() == super.tileY)) {
			if(game.getMode() % 2 == 1) {
				if(this.anim != this.dead) {
					game.addFloatingText(new FloatingText(positionX, positionY - 6, "+20", java.awt.Color.RED.getRGB()));
					game.setScoreCount(game.getScoreCount() + 20);
				}
				this.anim = this.dead;
			    this.rect = this.rDead;
			} else {
			    game.setMode(6);
			}
		}
		
		if((this.tileX % 2 == 1) && (this.tileY % 2 == 1) && (this.positionX % PacmanGameManager.TILE_SIZE == PacmanGameManager.TILE_SIZE / 2) && (this.positionY % PacmanGameManager.TILE_SIZE == PacmanGameManager.TILE_SIZE / 2) && (this.random.nextBoolean())) { 
			this.wantedRotation = this.random.nextInt(4);
			if(this.wantedRotation == (this.rotation + 2) % 4) this.wantedRotation = this.rotation; 
		}
		
		if((game.getMode() != 2) && (game.getMode() != 3)) {
		    this.anim.setCurrentTile(0);
	    } else {
			if(this.rotation != this.wantedRotation) {
                if((this.wantedRotation + 2) % 4 == this.rotation) this.rotation = this.wantedRotation;
                
                if((this.positionX % PacmanGameManager.TILE_SIZE == PacmanGameManager.TILE_SIZE / 2) && (this.positionY % PacmanGameManager.TILE_SIZE == PacmanGameManager.TILE_SIZE / 2)) { 
			    	switch(this.wantedRotation) {
	                case 0: 
	                	if(game.getWalkable()[super.tileX + 1][super.tileY]) this.rotation = 0;
	    		    	break;
	    		    case 1: 
	    		    	if(game.getWalkable()[super.tileX][super.tileY + 1]) this.rotation = 1;
	    		    	break;
	    		    case 2: 
	    		    	if(game.getWalkable()[super.tileX - 1][super.tileY]) this.rotation = 2;
	    		    	break;
	    		    case 3: 
	    		    	if(game.getWalkable()[super.tileX][super.tileY - 1]) this.rotation = 3;
	    		    	break;
	    		    default: break;
	                }
		        }
			}
			
			updateCounter++;
			updateCounter %= 3;
			
			if(updateCounter != 0)
			switch(this.rotation) {
		    case 0: 
		    	if(game.getWalkable()[super.tileX + 1][super.tileY] || (this.positionX % PacmanGameManager.TILE_SIZE < PacmanGameManager.TILE_SIZE / 2)) {
                	this.anim.setCurrentTile(1);
		    		super.positionX += STEP; 
		    	} else {
		    		this.wantedRotation = this.random.nextInt(4);
		    		if(this.wantedRotation == this.rotation) this.wantedRotation = this.random.nextInt(4); 
		    	}
		    	break;
		    case 1: 
		    	if(game.getWalkable()[super.tileX][super.tileY + 1] || (this.positionY % PacmanGameManager.TILE_SIZE < PacmanGameManager.TILE_SIZE / 2)) {
                	this.anim.setCurrentTile(2);
		    		super.positionY += STEP; 
		    	} else { 
		    		this.wantedRotation = this.random.nextInt(4);
		    		if(this.wantedRotation == this.rotation) this.wantedRotation = this.random.nextInt(4); 
		    	}
		    	break;
		    case 2: 
		    	if(game.getWalkable()[super.tileX - 1][super.tileY] || (this.positionX % PacmanGameManager.TILE_SIZE > PacmanGameManager.TILE_SIZE / 2)) {
                	this.anim.setCurrentTile(3);
		    		super.positionX -= STEP; 
		    	} else {
		    		this.wantedRotation = this.random.nextInt(4);
		    		if(this.wantedRotation == this.rotation) this.wantedRotation = this.random.nextInt(4); 
		    	}
		    	break;
		    case 3: 
		    	if(game.getWalkable()[super.tileX][super.tileY - 1] || (this.positionY % PacmanGameManager.TILE_SIZE > PacmanGameManager.TILE_SIZE / 2)) {
                	this.anim.setCurrentTile(4);
		    		super.positionY -= STEP; 
		    	} else {
		    		this.wantedRotation = this.random.nextInt(4);
		    		if(this.wantedRotation == this.rotation) this.wantedRotation = this.random.nextInt(4); 
		    	}
		    	break;
		    default: break;
		    }
		}
		
		super.recalculateTiles();
		
		if((game.getPacman().getTileX() == super.tileX) && (game.getPacman().getTileY() == super.tileY)) {
			if(game.getMode() % 2 == 1) {
				if(this.anim != this.dead) {
					game.addFloatingText(new FloatingText(positionX, positionY - 6, "+20", java.awt.Color.RED.getRGB()));
					game.setScoreCount(game.getScoreCount() + 20);
				}
				this.anim = this.dead;
			    this.rect = this.rDead;
			} else {
			    game.setMode(6);
			}
		}
		
		if(game.getMode() % 2 == 0) {
			this.anim = this.normal;
			this.rect = this.rNormal;
			this.eatable.setCurrentTile(0);
			this.dead.setCurrentTile(0);
		} else if(this.anim != this.dead){
			this.anim = this.eatable;
			this.rect = this.rEatable;
			this.normal.setCurrentTile(0);
		}
		
	}

	@Override
	public void render(Renderer renderer) {
		
		renderer.add(this.anim, ((int)super.positionX) - this.anim.getWidth() / 2, ((int)super.positionY) - this.anim.getHeight() / 2, GHOST_LAYER);
		if(game.isDebug()) renderer.add(this.rect, PacmanGameManager.TILE_SIZE * super.tileX, PacmanGameManager.TILE_SIZE * super.tileY, PacmanGameManager.POSITION_RECTANGLE_LAYER);
		
	}
	
}

