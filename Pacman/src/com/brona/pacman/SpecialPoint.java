package com.brona.pacman;

import com.brona.gameengine.Input;
import com.brona.gameengine.Renderer;
import com.brona.gameengine.renderables.Image;
import com.brona.gameengine.renderables.Rectangle;

public class SpecialPoint extends GameObject {

	public static final int POINT_LAYER = 3;
	public static final float WAITING_TIME = 10;
	
	private Image img;
	private Rectangle rect;
	
	public SpecialPoint(float positionX, float positionY) {
		super(positionX, positionY);
		
		img = new Image("/pacman/images/special_point.png", true);
	
		this.rect = new Rectangle(PacmanGameManager.TILE_SIZE, PacmanGameManager.TILE_SIZE, (java.awt.Color.GREEN.getRGB() & 0x00ffffff) | 0x77000000, true);
		
	}

	private boolean changed;
	private float waitTime;
	
	@Override
	public void update(Input input, float deltaTime) {
		
		if(changed) {
		    if((game.getMode() == 2) || (game.getMode() == 3)) waitTime += deltaTime;
		    
		    if(waitTime >= WAITING_TIME) {
		    	if(game.getMode() % 2 == 1) game.setMode(game.getMode() - 1);
		    	this.dead = true;
		    }
		    
		} else {
			if((game.getPacman().getTileX() == super.tileX) && (game.getPacman().getTileY() == super.tileY)) {
				if(game.getMode() % 2 == 0) {
					changed = true;
					game.addFloatingText(new FloatingText(positionX, positionY - 6, "+10", java.awt.Color.RED.getRGB()));
			        game.setScoreCount(game.getScoreCount() + 10);
				    this.rect = new Rectangle(PacmanGameManager.TILE_SIZE, PacmanGameManager.TILE_SIZE, 0x77378e2b, true);
					game.setMode(game.getMode() + 1);
				}
			}
		}
	}

	@Override
	public void render(Renderer renderer) {
		if(!changed) renderer.add(this.img, ((int)super.positionX) - this.img.getWidth() / 2, ((int)super.positionY) - this.img.getHeight() / 2, POINT_LAYER);
		if(game.isDebug()) renderer.add(this.rect, PacmanGameManager.TILE_SIZE * super.tileX, PacmanGameManager.TILE_SIZE * super.tileY, PacmanGameManager.POSITION_RECTANGLE_LAYER);
	}
	
}
