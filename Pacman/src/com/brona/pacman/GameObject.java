package com.brona.pacman;

import com.brona.gameengine.Engine;
import com.brona.gameengine.Input;
import com.brona.gameengine.Renderer;

public abstract class GameObject {

	protected PacmanGameManager game;
	protected Engine engine;
	
	protected boolean dead = true;
	protected float positionX, positionY;
	protected int tileX, tileY;
	
	public GameObject(float positionX, float positionY) {
		this.positionX = positionX;
		this.positionY = positionY;
		
		this.tileX = (int)(positionX / PacmanGameManager.TILE_SIZE);
		this.tileY = (int)(positionY / PacmanGameManager.TILE_SIZE);
	
	    this.dead = false;
	}
	
	public void init(Engine engine, PacmanGameManager game) {
		this.game = game;
		this.engine = engine;
	}
	
	public abstract void update(Input input, float deltaTime);
	
	public abstract void render(Renderer renderer);
	
	protected void recalculateTiles() {
		this.tileX = (int)(this.positionX / PacmanGameManager.TILE_SIZE);
		this.tileY = (int)(this.positionY / PacmanGameManager.TILE_SIZE);
	}
	
	public boolean isDead() {
		return this.dead;
	}

	public float getPositionX() {
		return this.positionX;
	}

	public float getPositionY() {
		return this.positionY;
	}

	public int getTileX() {
		return this.tileX;
	}

	public int getTileY() {
		return this.tileY;
	}
	
}
