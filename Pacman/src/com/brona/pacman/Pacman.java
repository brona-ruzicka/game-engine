package com.brona.pacman;

import java.awt.event.KeyEvent;

import com.brona.gameengine.Input;
import com.brona.gameengine.Renderer;
import com.brona.gameengine.Renderer.Renderable;
import com.brona.gameengine.renderables.Animation;
import com.brona.gameengine.renderables.Rectangle;

public class Pacman extends GameObject {

	public static final float STEP = 1f;
	public static final int PACMAN_LAYER = 5;
	
	private Animation anim, normal, dead;
	private int rotation = 3;
	private int wantedRotation;
	
	private Rectangle rect;
	
	public Pacman(float positionX, float positionY) {
		super(positionX, positionY);
		this.normal = new Animation("/pacman/images/pacman_eating.png", true, 0.09f);
		this.dead = new Animation("/pacman/images/pacman_disapear.png", true, 0.13f); this.dead.setCurrentTile(9);
		this.anim = normal;
		this.rect = new Rectangle(PacmanGameManager.TILE_SIZE, PacmanGameManager.TILE_SIZE, (java.awt.Color.YELLOW.getRGB() & 0x00ffffff) | 0x77000000, true);
	}

	@Override
	public void update(Input input, float deltaTime) {
	
		boolean[] buttons = new boolean[4];
		int keyCount = 0;
		if(input.isKey(KeyEvent.VK_UP) || input.isKey(KeyEvent.VK_W))    { buttons[3] = true; keyCount++; }
		if(input.isKey(KeyEvent.VK_DOWN) || input.isKey(KeyEvent.VK_S))  { buttons[1] = true; keyCount++; }
		if(input.isKey(KeyEvent.VK_LEFT) || input.isKey(KeyEvent.VK_A))  { buttons[2] = true; keyCount++; }
		if(input.isKey(KeyEvent.VK_RIGHT) || input.isKey(KeyEvent.VK_D)) { buttons[0] = true; keyCount++; }

		if((!buttons[this.rotation]) && (keyCount == 1)) {
			if(buttons[0]) this.wantedRotation = 0; 
			if(buttons[1]) this.wantedRotation = 1; 
			if(buttons[2]) this.wantedRotation = 2; 
			if(buttons[3]) this.wantedRotation = 3;
		}		
		
		if((game.getMode() == 0) || (game.getMode() == 4)) {
			if(keyCount == 1) game.setMode(2);
			this.rotation = wantedRotation;
			this.anim.setCurrentTile(0);
		} else if((game.getMode() == 1) || (game.getMode() == 5)) {
			if(keyCount == 1) game.setMode(3);
			this.rotation = wantedRotation;
			this.anim.setCurrentTile(0);
		} else if((game.getMode() == 6) || (game.getMode() == 7)) {
			if(game.getPointsCount() == 0) {
				if(this.anim != this.normal) {
					this.anim = this.normal;
				}
				this.anim.setCurrentTile(0);
			} else {
				if(this.anim != this.dead) {
					this.anim = this.dead;
					this.anim.setCurrentTile(1);
				}
				if(this.anim.getCurrentTile() != 9) {
				    this.anim.update(deltaTime);
				}
			}
		} else {
			if(this.anim != this.normal) {
				this.anim = this.normal;
			}
			
			
			this.anim = this.normal;
            this.anim.update(deltaTime);
			
			if(this.rotation != this.wantedRotation) {
                if((this.wantedRotation + 2) % 4 == this.rotation) this.rotation = this.wantedRotation;
                
                if((this.positionX % PacmanGameManager.TILE_SIZE == PacmanGameManager.TILE_SIZE / 2) && (this.positionY % PacmanGameManager.TILE_SIZE == PacmanGameManager.TILE_SIZE / 2)) { 
			    	switch(this.wantedRotation) {
	                case 0: 
	                	if(game.getWalkable()[super.tileX + 1][super.tileY]) this.rotation = 0;
	    		    	break;
	    		    case 1: 
	    		    	if(game.getWalkable()[super.tileX][super.tileY + 1]) this.rotation = 1;
	    		    	break;
	    		    case 2: 
	    		    	if(game.getWalkable()[super.tileX - 1][super.tileY]) this.rotation = 2;
	    		    	break;
	    		    case 3: 
	    		    	if(game.getWalkable()[super.tileX][super.tileY - 1]) this.rotation = 3;
	    		    	break;
	    		    default: break;
	                }
		        }
			}
			
			switch(this.rotation) {
		    case 0: 
		    	if(game.getWalkable()[super.tileX + 1][super.tileY] || (this.positionX % PacmanGameManager.TILE_SIZE < PacmanGameManager.TILE_SIZE / 2)) {
		    	    super.positionX += STEP; 
		    	} else {
		    		
		    	}
		    	break;
		    case 1: 
		    	if(game.getWalkable()[super.tileX][super.tileY + 1] || (this.positionY % PacmanGameManager.TILE_SIZE < PacmanGameManager.TILE_SIZE / 2)) {
		    	    super.positionY += STEP; 
		    	}
		    	break;
		    case 2: 
		    	if(game.getWalkable()[super.tileX - 1][super.tileY] || (this.positionX % PacmanGameManager.TILE_SIZE > PacmanGameManager.TILE_SIZE / 2)) {
		    	    super.positionX -= STEP; 
		    	}
		    	break;
		    case 3: 
		    	if(game.getWalkable()[super.tileX][super.tileY - 1] || (this.positionY % PacmanGameManager.TILE_SIZE > PacmanGameManager.TILE_SIZE / 2)) {
		    	    super.positionY -= STEP; 
		    	}
		    	break;
		    default: break;
		    }
		}
		
		super.recalculateTiles();
	
	    if((game.getPoints()[super.tileX][super.tileY]) && ((this.positionX % PacmanGameManager.TILE_SIZE < 14) && (this.positionX % PacmanGameManager.TILE_SIZE > 2) && (this.positionY % PacmanGameManager.TILE_SIZE < 14) && (this.positionY % PacmanGameManager.TILE_SIZE > 2))) {
	    	game.getPoints()[super.tileX][super.tileY] = false;
	    	game.setPointsCount(game.getPointsCount() - 1);
	    	game.setScoreCount(game.getScoreCount() + 1);
	    }
	}

	@Override
	public void render(Renderer renderer) {
		renderer.add(Renderable.rotate(this.anim, this.rotation * 90), ((int)super.positionX) - this.anim.getWidth() / 2, ((int)super.positionY) - this.anim.getHeight() / 2, PACMAN_LAYER);
        if(game.isDebug()) renderer.add(this.rect, PacmanGameManager.TILE_SIZE * super.tileX, PacmanGameManager.TILE_SIZE * super.tileY, PacmanGameManager.POSITION_RECTANGLE_LAYER);
	}
}
